class SampleformsController < ApplicationController

  def index
    @sampleforms= Sampleform.all
  end


  def new
    @sampleforms = Sampleform.new
  end


  def create
    @sampleforms = Sampleform.new(params[:sampleform])
    if@sampleforms.save
      redirect_to sampleforms_path
    end
  end

  def destroy
    @sampleforms = Sampleform.find(params[:id])
    if @sampleforms.present?
      @sampleforms.destroy
    end
    redirect_to sampleforms_path
  end

end
